import { Component, OnInit } from '@angular/core';
import { GiphyService } from '../giphy.service';

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html',
  styleUrls: ['./galeria.component.css']
})
export class GaleriaComponent{
  search;
  gifs = [];
  obj =[];

  searchGif(word, $event){
    $event.preventDefault();
    this.search = word.value;
    console.log(this.search);
    this.giphyService.getViewData(this.search).subscribe(res => {
      this.gifs = res["data"];
    });
  }
 
  constructor( private giphyService:GiphyService){
    
  };


}

