import { Injectable } from '@angular/core';
import { getViewData } from '@angular/core/src/render3/instructions';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class GiphyService {

  constructor(private httpClient:HttpClient) { 
    console.log('service is working'); 
  }
    
  getViewData(search){
      return this.httpClient.get('//api.giphy.com/v1/gifs/search?q='+search+'&api_key=dvTJIgL2cvpdL4o2UTlK2LcHPbjqlMgX&limit=12');
  } 
}
